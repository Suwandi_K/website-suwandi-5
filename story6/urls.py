from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='story6-home'),
    path('<int:indexkegiatan>/', views.tambahpeserta, name='story6-peserta'),
]