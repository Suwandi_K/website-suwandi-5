from django.test import TestCase
from django.test import Client
from django.urls import resolve

from story6.models import Kegiatan, Peserta
from . import views


# Create your tests here.

class UnitTestForStory6(TestCase):
    def test_response_page(self):
        response = Client().get('/kegiatan/')
        self.assertEqual(response.status_code, 200)

    def test_template_used(self):
        response = Client().get('/kegiatan/')
        self.assertTemplateUsed(response, 'story6.html')

    def test_func_page(self):
        found = resolve('/kegiatan/')
        self.assertEqual(found.func, views.index)
