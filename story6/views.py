from django.shortcuts import render , redirect
from .forms import KegiatanForm , PesertaForm
from .models import Kegiatan , Peserta

# Create your views here.
def index(request):
    form = KegiatanForm()
    if request.method == 'POST':
        forminput = KegiatanForm(request.POST)
        if forminput.is_valid():
            data = forminput.cleaned_data
            kegiatan = Kegiatan()
            this_data = Kegiatan.objects.all()
            kegiatan.nama_kegiatan = data['nama_kegiatan']
            kegiatan.save()
            return render(request, 'story6.html', {'form':form, 'status': 'success', 'data' : this_data})
        else:
            this_data = Kegiatan.objects.all()
            return render(request, 'story6.html', {'form':form, 'status': 'failed', 'data' : this_data})
    else:
        this_data = Kegiatan.objects.all()
        return render(request, 'story6.html', {'form':form, 'data':this_data})

def tambahpeserta(request, indexkegiatan = 0):
    form = PesertaForm()
    if indexkegiatan != 0 and Kegiatan.objects.filter(pk = indexkegiatan).exists():
        this_data = Kegiatan.objects.get(pk = indexkegiatan)
        data1 = Peserta.objects.filter(kegiatan = this_data)
        if request.method == 'POST' and request.POST.get('action')=='delete':
            data1.delete()
            return redirect(tambahpeserta)
        elif request.method == 'POST' and request.POST.get('action')!='delete':
            forminput = PesertaForm(request.POST)
            if forminput.is_valid():
                data = forminput.cleaned_data
                peserta = Peserta()
                peserta.nama_peserta = data['nama_peserta']
                peserta.kegiatan = this_data
                peserta.save()
                return render(request, 'peserta.html', {'form':form, 'status': 'success', 'data' : this_data , 'data1' : data1})
            else:
                return render(request, 'peserta.html', {'form':form, 'status': 'failed', 'data' : this_data, 'data1': data1})
        else:
            return render(request, 'peserta.html', {'form': form, 'data': this_data , 'data1' : data1})
    else:
        this_data = Kegiatan.objects.all()
        return render(request, 'kegiatan.html', {'form': form, 'data': this_data})