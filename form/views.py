from django.shortcuts import render, redirect
from .forms import JadwalForm
from .models import Jadwal , Challenge
import datetime

# Create your views here.
def index(request):
    form = JadwalForm()
    if request.method == 'POST':
        forminput = JadwalForm(request.POST)
        if forminput.is_valid():
            data = forminput.cleaned_data
            jadwal_kuliah = Jadwal()
            this_data = Jadwal.objects.all()
            jadwal_kuliah.nama_matkul = data['nama_matkul']
            jadwal_kuliah.nama_dosen = data['nama_dosen']
            if data['jumlah_sks'] > 0 :
                jadwal_kuliah.jumlah_sks = data['jumlah_sks']
            else :
                return render(request, 'jadwal.html', {'form':form, 'status': 'failed_value', 'data' : this_data})
            jadwal_kuliah.deskripsi = data['deskripsi']
            jadwal_kuliah.semester_tahun = data['semester_tahun']
            jadwal_kuliah.ruang_kelas = data['ruang_kelas']
            jadwal_kuliah.save()
            return render(request, 'jadwal.html', {'form':form, 'status': 'success', 'data' : this_data})
        else:
            this_data = Jadwal.objects.all()
            return render(request, 'jadwal.html', {'form':form, 'status': 'failed', 'data' : this_data})
    else:
        this_data = Jadwal.objects.all()
        return render(request, 'jadwal.html', {'form':form, 'data':this_data})


def detailMatkul(request, indexmatkul = 0):
    if indexmatkul != 0 and Jadwal.objects.filter(pk=indexmatkul).exists():
        this_data = Jadwal.objects.get(pk = indexmatkul)
        data_tugas = Challenge.objects.filter(nama_matkul = this_data).order_by("deadline")
        data_tugas_new = []
        for x in data_tugas :
            time_delta = x.deadline-datetime.datetime.now(datetime.timezone.utc)
            print(time_delta.days)
            if(time_delta.days*86400+time_delta.seconds<86400):
                color = "#fe1203"
            elif(time_delta.days*86400+time_delta.seconds<259200):
                color = "#ffba00"
            else:
                color = "#031cfe"
            data_tugas_new.append((x,color))

        if request.method == 'POST' and request.POST.get('action')=='delete':
            this_data.delete()
            return redirect(index)
        return render(request, 'detail_jadwal.html', {'data':this_data})
    else:
        form = JadwalForm()
        this_data = Jadwal.objects.all()
        return render(request, 'jadwal.html', {'form':form, 'data':this_data})