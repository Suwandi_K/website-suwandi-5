from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='story5-form'),
    path('<int:indexmatkul>/',views.detailMatkul, name='detail matkul jadwal'),
]