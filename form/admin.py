from django.contrib import admin
from .models import Jadwal, Challenge
# Register your models here.

admin.site.register(Jadwal)
admin.site.register(Challenge)