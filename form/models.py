from django.db import models
from datetime import datetime 

# Create your models here.
class Jadwal(models.Model):
    nama_matkul = models.CharField(max_length = 100)
    nama_dosen = models.CharField(max_length = 100)
    jumlah_sks = models.IntegerField()
    deskripsi = models.TextField()
    semester_tahun = models.CharField(max_length = 50, null=True)
    ruang_kelas = models.CharField(max_length = 20)
    
    def __str__(self):
        return self.nama_matkul

class Challenge(models.Model):
    nama_tugas = models.CharField(max_length = 64)
    nama_matkul = models.ForeignKey(Jadwal, on_delete=models.CASCADE)
    deskripsi = models.TextField(blank=True)
    deadline = models.DateTimeField()
    def __str__(self):
        return self.nama_tugas + self.deadline.strftime(" %d %m %Y, %H:%M:%S ")
