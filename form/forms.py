from django import forms

class JadwalForm(forms.Form):
    CHOICES = [
        ('Gasal 2019/2020','Gasal 2019/2020'),
        ('Genap 2019/2020','Genap 2019/2020'),
        ('Gasal 2020/2021','Gasal 2020/2021'),
        ('Genap 2020/2021','Genap 2020/2021'),
    ]
    nama_matkul = forms.CharField(label='Nama Mata Kuliah', max_length=100,widget=forms.TextInput(attrs={'class':'form-control'}))
    nama_dosen = forms.CharField(label='Nama Dosen', max_length=100, widget=forms.TextInput(attrs={'class':'form-control'}))
    jumlah_sks = forms.IntegerField(label='Jumlah SKS', widget=forms.NumberInput(attrs={'class':'form-control'}))
    deskripsi = forms.CharField(widget=forms.Textarea(attrs={'class':'form-control'}))
    semester_tahun = forms.ChoiceField(choices=CHOICES)
    ruang_kelas = forms.CharField(label='Ruang Kelas', max_length=20, widget=forms.TextInput(attrs={'class':'form-control'}))
